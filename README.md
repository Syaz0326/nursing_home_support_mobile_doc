# 介護情報入力システム(モバイルアプリケーション)

## 概要
卒業研究で作成する介護情報入力システムのモバイルアプリケーションに関するドキュメント
cordovaを用いてios android 両対応のアプリケーション作成を行う

## 使用ライブラリ・フレームワーク
* [Node.js](https://nodejs.org/ja/)
* [cordova](https://cordova.apache.org/)
