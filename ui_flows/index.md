# UI Flows

## 概要
アプリケーションの全体の画面遷移や細かな画面遷移を[UI Flow](https://github.com/hirokidaichi/uiflow)で表したもの

## 使用ツール
* [guiflow](https://qiita.com/hirokidaichi/items/ff54a968bdd7bcc50d42)
